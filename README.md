# sTGC Noise and Connectivity Measurements (with plots and mapping)

## Package contents ##

```
CMakeLists.txt
README.txt
setupEnv.sh
stgc_stdev_plots
```

## Setup ##

1) Source the setup script and configure the cmake files.

```
source setupEnv.sh
cmake_config
```

2) Compile the executibles and create the binaries.

```
cd x86_64-centos7-gcc8-opt
make install
```

## Subsequent compilations ##

After following the setup procedure, you only need to run ``make`` when recompiling.
